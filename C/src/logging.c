/**
*
*
*/

/* The system headers */
#include <stdio.h>
/* Custom Headers */
#include "logging.h"

/* The System Log for all system events */
void systemLog( char message[] );
/* Stage of logging functions */
void resultsLog( char message[] );

/* 
* The "Main" of the logging.c file. To add in other types of logging
* just add in another swtich and case with another staged function, and 
* actual function listed below the logger function. 
*/
void logger( char message[], int logType )
{
	switch( logType )
	{
		case 1 :
			systemLog( message );
			break;
		case 2 :
			resultsLog( message );
			break;
		default :
			systemLog( message );
	}

	return;
}

void systemLog( char message[] )
{
	FILE *logFile;
	logFile = fopen( "system.log", "a" );
	fprintf( logFile, message );
	//fprintf( logFile, "\n" );
	fclose( logFile );

	return;
}

void resultsLog( char message[] )
{
	FILE *logFile;
	logFile = fopen( "results.log", "a" );
	fprintf( logFile, message );
	//fputs( message, logFile );
	//fprintf( logFile, "\n" );
	fclose( logFile );

	return;
}