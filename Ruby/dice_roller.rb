#!/usr/bin/ruby 
=begin

=end

module Die
	class DiceRoller
		def self.write_results_log( result )
			# Take in the string, and write to the results log file
			results_log = File.open("results.log", "a")
			results_log.write("#{result}\n")
			results_log.close
		end

		def self.roll_dice( dice = 1, faces = 6 )
			# Pass the number of dice, and the number of faces to be rolled.
			roll = 0
			dicenum = dice.to_i
			facenum = faces.to_i

			puts "Rolling a #{dicenum}d#{facenum}"

			for i in 1..dicenum do
				result = 1 + rand(facenum)
				puts result
				write_results_log( result )
				roll = roll + result
			end
			
			roll_final = "Total: #{roll}"
			puts roll_final
			write_results_log( roll_final )
		end

		def self.main
			# This is the infinite loop. Keep going until the CTRL+C is met
			# Or the character of [q|Q] is input as the type of dice.
			option = true
			while option == true do 
				puts "Enter number of dice, or q|Q to quit"
				dice = gets.chomp()
				if "#{dice}" == "q" || "#{dice}" == "Q" then
					break
				end
				puts "Enter the type of dice. 2, 4, 6, and so forth"
				faces = gets.chomp()
				puts "Rolling the Dice!"
				roll_dice( dice, faces )
			end
			puts "Ending Program"
		end
	end
end

Die::DiceRoller.main()