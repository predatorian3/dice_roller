/**
*
*
*/

// The System includes
#include <stdio.h>
#include <stdlib.h> // For the random numbers

// The Custom Headers
#include "logging.h"
#include "roller.h"

void roller( int rolls, int dieSides )
{
	// 
	int total = 0;
	printf( "Rolling the dice!\n" );
	printf( "Rolling a %dd%d \n", rolls, dieSides );
	char message[10];
	for( int i = 0; i < rolls; i++ )
	{
		int result = 1 + rand() % dieSides;
		printf( "Rolled: %d \n", result );
		snprintf( message, 10, "%d\n", result ); // Convert Int to a String
		logger( message, 1 );
		logger( message, 2 );
		total = total + result;
	}
	printf( "Total: %d \n", total );
	snprintf( message, 10, "%d\n", total );
	logger( message, 1 );
	logger( message, 2 );

	return;
}

